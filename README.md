# Petflix

This project is an Api built with Node.js and uses Docker for containerization. It includes a set of APIs for user authentication and movie recommendations.

### Getting Started

Prerequisites

You will need to have the following tools installed on your local machine:


Docker

Node.js

### Installation

1. Clone the repository to your local machine

2. Navigate to the project directory and run `docker-compose up -d`

3. Run `npm run migrate:dev` or `npm run migrate:reset` to set up the database

4. Run `npm run fill-db` to seed the database with sample data

5. Finally, run `npm run dev` to start the server

### Usage

#### Auth Routes

`POST /auth/signin` - allows a user to sign in to their account

`POST /auth/signup` - allows a user to create a new account

#### Movie Routes

`GET /movies` - returns a list of movies based on pagination and search query parameters. Query parameters: page - number of page, limit - limit of page, query - search string by movie title

`GET /movies/search` - returns a list of movies based on a search query parameter. Query parameters: query - search movies by title and description

`GET /movies/recommended` - returns a list of recommended movies for the current user

`POST /movies/like` - allows the current user to like a movie

`GET /movies/:id` - returns details about a specific movie, identified by id

