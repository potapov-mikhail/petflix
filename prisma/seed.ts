import { PrismaClient, Prisma } from '@prisma/client';
import { authService } from '../src/domains/auth';

const prisma = new PrismaClient();

const usersData: Prisma.UserCreateInput[] = [
	{
		email: 'test@test.ru',
		password: 'test',
	},
	{
		email: 'test1@test.ru',
		password: 'test',
	},
	{
		email: 'test2@test.ru',
		password: 'test',
	},
];

async function main(): Promise<void> {
	for (const user of usersData) {
		await authService.register(user.email, user.password);
		console.log('Users created', user);
	}
}

main()
	.then(async () => {
		await prisma.$disconnect();
	})
	.catch(async (e) => {
		console.error(e);
		await prisma.$disconnect();
		process.exit(1);
	});
