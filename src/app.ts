import express, { Express } from 'express';
import { routes } from './routes';
import { auth } from './domains/auth';
import { errorHandler } from './common/errors';

export const createApplication = (): Express => {
	const app = express();

	app.use(express.json());
	app.use(auth);
	app.use(routes);
	app.use(errorHandler);

	return app;
};
