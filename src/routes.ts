import { Router } from 'express';
import { authGuard } from './domains/auth/auth';
import { routes as authRoutes } from './domains/auth';
import { routes as moviesRoutes } from './domains/movies';
import { healthController, notFoundController } from './common/controllers';

const routes = Router();

routes.use('/health', healthController);
routes.use(authRoutes);
routes.use(authGuard, moviesRoutes);
routes.use('*', notFoundController);

export { routes };
