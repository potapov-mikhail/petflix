import { NextFunction, Request, Response } from 'express';
import { HTTPError, InternalServerError } from './http-error.class';

export function errorHandler(
	error: unknown,
	req: Request,
	res: Response,
	next: NextFunction,
): void {
	let err: HTTPError;

	if (error instanceof HTTPError) {
		err = error;
		console.error(`[${err.statusCode}]${err.message}`);
	} else {
		err = new InternalServerError();
		console.error(`[${err.statusCode}]${err.message}: error: ${error}`);
	}

	res.status(err.statusCode).json({
		errorMessage: err.message,
	});
}
