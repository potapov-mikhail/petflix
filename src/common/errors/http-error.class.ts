export class HTTPError extends Error {
	statusCode: number;

	constructor(statusCode: number, message: string) {
		super(message);
		this.statusCode = statusCode;
		this.message = message;
	}
}

export class BadRequestError extends HTTPError {
	constructor(message?: string) {
		super(400, message ?? 'Bad Request Error');
	}
}

export class UnauthorizedError extends HTTPError {
	constructor(message?: string) {
		super(401, message ?? 'Unauthorized Error');
	}
}

export class NotFoundError extends HTTPError {
	constructor(message?: string) {
		super(404, message ?? 'Not Found Error');
	}
}

export class ConflictError extends HTTPError {
	constructor(message?: string) {
		super(409, message ?? 'Conflict Error');
	}
}

export class InternalServerError extends HTTPError {
	constructor(message?: string) {
		super(500, message ?? 'Internal Server Error');
	}
}
