export interface IPagination {
	page: number;
	limit: number;
	total: number;
}

export interface IDataWithPagination<T> {
	items: T[];
	pagination: IPagination;
}

export interface IPaginationParams {
	query?: string;
	page?: number;
	limit?: number;
}
