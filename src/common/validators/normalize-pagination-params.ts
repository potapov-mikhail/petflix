import { IPaginationParams } from '../types';

export const normalizePaginationParams = (
	pagination: IPaginationParams,
): Required<IPaginationParams> => {
	const result: Required<IPaginationParams> = {
		page: 1,
		limit: 20,
		query: '',
	};

	if (pagination.page && pagination.page >= 1) {
		result.page = pagination.page;
	}

	if (pagination.limit && pagination.limit >= 20 && pagination.limit <= 100) {
		result.limit = pagination.limit;
	}

	if (pagination.query?.trim()) {
		result.query = pagination.query.trim();
	}

	return result;
};
