import { Request, Response } from 'express';

export const healthController = (req: Request, res: Response): void => {
	res.json({
		status: 'ok',
	});
};
