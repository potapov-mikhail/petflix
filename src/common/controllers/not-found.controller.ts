import { NextFunction, Request, Response } from 'express';
import { NotFoundError } from '../errors';

export function notFoundController(req: Request, res: Response, next: NextFunction): void {
	next(new NotFoundError('Route not found'));
}
