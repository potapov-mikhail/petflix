import { z } from 'zod';
import { config, DotenvConfigOutput, DotenvParseOutput } from 'dotenv';

export class ConfigService {
	private readonly config: DotenvParseOutput;

	constructor() {
		const result: DotenvConfigOutput = config();

		if (result.error) {
			console.error('Failed to read .env file or is missing');
			throw result.error;
		} else {
			this.config = result.parsed as DotenvParseOutput;
			this.validateConfig(this.config);
			console.log('Successfully read .env file');
		}
	}

	get(key: string): string {
		return this.config[key];
	}

	private validateConfig(env: NodeJS.ProcessEnv): void {
		const schema = z.object({
			TZ: z.string(),
			POSTGRES_DB: z.string(),
			POSTGRES_USER: z.string(),
			POSTGRES_PASSWORD: z.string(),
			POSTGRES_PORT: z.string(),
			DATABASE_URL: z.string(),
			STACK_VERSION: z.string(),
			ES_PORT: z.string(),
			ES_URL: z.string(),
			KIBANA_PORT: z.string(),
			AUTH_SALT: z.string(),
			AUTH_SECRET: z.string(),
			NEO4J_PORT: z.string(),
			NEO4J_USER: z.string(),
			NEO4J_PASSWORD: z.string(),
			NEO4J_URL: z.string(),
		});

		const result = schema.safeParse(env);

		if (!result.success) {
			throw new SyntaxError(`.env -> ${result.error.message}`);
		}
	}
}

export const configService = new ConfigService();
