import neo4j from 'neo4j-driver';
import { Driver } from 'neo4j-driver/types/driver';
import { configService } from '../config';

export class Neo4jService {
	readonly client: Driver;

	constructor() {
		this.client = neo4j.driver(
			configService.get('NEO4J_URL'),
			neo4j.auth.basic(configService.get('NEO4J_USER'), configService.get('NEO4J_PASSWORD')),
		);
	}

	async disconnect(): Promise<void> {
		await this.client.close();
		console.log('Successfully disconnected from the neo4j database');
	}
}

export const neo4jService = new Neo4jService();
