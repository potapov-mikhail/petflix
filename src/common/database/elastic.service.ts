import { Client } from '@elastic/elasticsearch';
import { configService } from '../config';

export class ElasticService {
	readonly client: Client;

	constructor() {
		this.client = new Client({ node: configService.get('ES_URL') });
	}

	async disconnect(): Promise<void> {
		await this.client.close();
		console.log('Successfully disconnected from the elasticsearch');
	}
}

export const elasticService = new ElasticService();
