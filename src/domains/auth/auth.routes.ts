import { Router } from 'express';
import { authDtoSchema } from './auth.dto';
import { bodyValidator } from '../../common/validators';
import { signInController, signUpController } from './auth.controller';

const routes = Router();

routes.post('/auth/signin', bodyValidator(authDtoSchema), signInController);
routes.post('/auth/signup', bodyValidator(authDtoSchema), signUpController);

export { routes };
