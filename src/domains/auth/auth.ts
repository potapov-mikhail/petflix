import { NextFunction, Request, Response } from 'express';
import { authService } from './auth.service';
import { UnauthorizedError } from '../../common/errors';

export const auth = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
	if (req.headers.authorization) {
		try {
			const token = req.headers.authorization.replace('Bearer ', '');
			const { payload } = await authService.verifyJWT(token);
			req.userId = payload.userId;
			next();
		} catch (e) {
			next(e);
		}
	} else {
		next();
	}
};

export const authGuard = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
	next(req.userId ? undefined : new UnauthorizedError());
};
