import { compare, hash } from 'bcryptjs';
import { sign, verify } from 'jsonwebtoken';
import { configService } from '../../common/config';
import { prismaService } from '../../common/database';
import { BadRequestError, ConflictError, NotFoundError } from '../../common/errors';

export interface JWT<T> {
	payload: { userId: number };
}

export class AuthService {
	private readonly salt: number;
	private readonly secret: string;

	constructor() {
		this.salt = Number(configService.get('AUTH_SALT'));
		this.secret = configService.get('AUTH_SECRET');
	}

	async login(email: string, password: string): Promise<string> {
		const user = await prismaService.client.user.findUnique({ where: { email } });

		if (!user) {
			throw new NotFoundError('User not found');
		}

		const isCompare = await compare(password, user.password);

		if (!isCompare) {
			throw new BadRequestError('Incorrect password');
		}

		return this.signJWT({ userId: user.id });
	}

	async register(email: string, password: string): Promise<string> {
		const user = await prismaService.client.user.findUnique({ where: { email } });

		if (user) {
			throw new ConflictError('Email already registered');
		}

		const passwordHash = await this.getPasswordHash(password);
		const createdUser = await prismaService.client.user.create({
			data: { email, password: passwordHash },
		});

		return this.signJWT({ userId: createdUser.id });
	}

	async verifyJWT<T>(jwt: string): Promise<JWT<T>> {
		return new Promise((resolve, reject) => {
			verify(jwt, this.secret, (err, payload) => {
				if (err) {
					return reject(err);
				}

				resolve(payload as JWT<T>);
			});
		});
	}

	async signJWT(payload: unknown): Promise<string> {
		return new Promise<string>((resolve, reject) => {
			sign(
				{
					payload,
				},
				this.secret,
				{
					algorithm: 'HS256',
				},
				(err, token) => {
					if (err) {
						reject(err);
					}
					resolve(token as string);
				},
			);
		});
	}

	private async getPasswordHash(password: string): Promise<string> {
		return hash(password, this.salt);
	}
}

export const authService = new AuthService();
