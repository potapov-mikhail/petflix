import { NextFunction, Request, Response } from 'express';
import { AuthDto } from './auth.dto';
import { authService } from './auth.service';

export const signInController = async (
	req: Request,
	res: Response,
	next: NextFunction,
): Promise<void> => {
	const { email, password } = req.body as AuthDto;

	try {
		const jwt = await authService.login(email, password);

		res.status(200).json({ jwt });
	} catch (e) {
		next(e);
	}
};

export const signUpController = async (
	req: Request,
	res: Response,
	next: NextFunction,
): Promise<void> => {
	const { email, password } = req.body as AuthDto;

	try {
		const jwt = await authService.register(email, password);

		res.status(200).json({ jwt });
	} catch (e) {
		next(e);
	}
};
