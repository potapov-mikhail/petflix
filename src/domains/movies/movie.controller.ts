import { NextFunction, Request, Response } from 'express';
import { moviesService } from './movie.service';
import { IPaginationParams } from '../../common/types';

export const getMoviesController = async (
	req: Request,
	res: Response,
	next: NextFunction,
): Promise<void> => {
	try {
		const { page, limit, query } = req.query;
		const findParams: IPaginationParams = {
			...(Number.isSafeInteger(Number(page)) ? { page: Number(page) } : {}),
			...(Number.isSafeInteger(Number(limit)) ? { limit: Number(limit) } : {}),
			...(typeof query === 'string' && query.trim().length ? { query: query.trim() } : {}),
		};

		const data = await moviesService.getMovies(findParams);

		res.status(200).json(data);
	} catch (e) {
		next(e);
	}
};

export const getMovieByIdController = async (
	req: Request,
	res: Response,
	next: NextFunction,
): Promise<void> => {
	try {
		const id = Number(req.params['id']);
		const movie = await moviesService.getMovieById(id);

		res.status(200).json({
			data: movie,
		});
	} catch (e) {
		next(e);
	}
};

export const searchMovieController = async (
	req: Request,
	res: Response,
	next: NextFunction,
): Promise<void> => {
	try {
		const query = req.query.query as string;
		const movies = await moviesService.searchMovie(query);

		res.status(200).json({
			data: movies,
		});
	} catch (e) {
		next(e);
	}
};

export const likeMovieController = async (
	req: Request,
	res: Response,
	next: NextFunction,
): Promise<void> => {
	try {
		const userId = req.userId;
		const movieId = Number(req.body.movieId);
		await moviesService.likeMovie(userId, movieId);

		res.status(200).send();
	} catch (e) {
		next(e);
	}
};

export const getRecommendedMoviesController = async (
	req: Request,
	res: Response,
	next: NextFunction,
): Promise<void> => {
	try {
		const userId = req.userId;
		const data = await moviesService.getRecommendedMovies(userId);

		res.status(200).json({
			data,
		});
	} catch (e) {
		next(e);
	}
};
