import { Router } from 'express';
import {
	getMovieByIdController,
	getMoviesController,
	getRecommendedMoviesController,
	likeMovieController,
	searchMovieController,
} from './movie.controller';
import { likeMovieDtoSchema } from './movie.dto';
import { bodyValidator } from '../../common/validators';

const routes = Router();

routes.get('/movies', getMoviesController);
routes.get('/movies/search', searchMovieController);
routes.get('/movies/recommended', getRecommendedMoviesController);
routes.post('/movies/like', bodyValidator(likeMovieDtoSchema), likeMovieController);
routes.get('/movies/:id', getMovieByIdController);

export { routes };
