import { z } from 'zod';

export const likeMovieDtoSchema = z.object({
	movieId: z.string(),
});

export type LikeMovieDto = z.infer<typeof likeMovieDtoSchema>;
