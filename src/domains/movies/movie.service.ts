import { Movie, Prisma } from '@prisma/client';
import { NotFoundError } from '../../common/errors';
import { IDataWithPagination, IPaginationParams } from '../../common/types';
import { elasticService, neo4jService, prismaService } from '../../common/database';
import { normalizePaginationParams } from '../../common/validators';

export class MoviesService {
	private readonly elasticIndex = 'movies';

	async getMovieById(id: number): Promise<Movie | null> {
		return prismaService.client.movie.findUnique({ where: { id } });
	}

	async getMovies(params: IPaginationParams = {}): Promise<IDataWithPagination<Movie>> {
		const { page, limit, query } = normalizePaginationParams(params);
		const skip = (page - 1) * limit;
		const take = limit;

		let where: Prisma.MovieWhereInput = {};

		if (query) {
			where = { title: { contains: query, mode: 'insensitive' } };
		}

		const [total, items] = await prismaService.client.$transaction([
			prismaService.client.movie.count({ where }),
			prismaService.client.movie.findMany({
				where,
				skip,
				take,
			}),
		]);

		return { items, pagination: { page, limit, total: Math.ceil(total / limit) } };
	}

	async putMovieToSearch(movie: Movie): Promise<void> {
		await elasticService.client.index({
			index: this.elasticIndex,
			document: movie,
		});
	}

	async searchMovie(query: string): Promise<Movie[]> {
		const result = await elasticService.client.search({
			index: this.elasticIndex,
			size: 8,
			query: {
				multi_match: {
					query: query + '~5',
					fields: ['title', 'description'],
				},
			},
		});

		return result.hits.hits.map((hit) => hit._source as Movie);
	}

	async getRecommendedMovies(userId: number): Promise<{ title: string; movieId: number }[]> {
		const session = neo4jService.client.session();

		try {
			const data = await session.run(
				`MATCH (me:User)-[:LIKE]->(m:Movie)<-[:LIKE]-(u:User)-[:LIKE]->(rec:Movie)
							 WHERE me.userId=$userId
							 AND NOT (me)-[:LIKED]->(rec)
               RETURN rec, COUNT(*) AS matches
               ORDER BY matches DESC LIMIT 25`,
				{
					userId,
				},
			);

			session.close();
			return data.records.map((record) => record['_fields'][0]['properties']);
		} catch (e) {
			session.close();
			throw e;
		}
	}

	async likeMovie(userId: number, movieId: number): Promise<void> {
		const movie = await this.getMovieById(movieId);

		if (!movie) {
			throw new NotFoundError();
		}

		const session = neo4jService.client.session();

		try {
			const likeRelation = await session.run(
				'MATCH(m:Movie {title:$title, movieId:$movieId})-[r]-(u:User {userId:$userId}) RETURN r',
				{
					userId,
					title: movie.title,
					movieId: movie.id,
				},
			);

			const isLikeRelationExist = likeRelation.records.length > 0;

			if (isLikeRelationExist) {
				return;
			}

			const movieNode = await session.run(
				'MATCH(m:Movie) WHERE m.movieId=$movieId AND m.title=$title RETURN m LIMIT 1',
				{ title: movie.title, movieId: movie.id },
			);

			const userNode = await session.run('MATCH(u:User) WHERE u.userId=$userId RETURN u LIMIT 1', {
				userId: userId,
			});

			const isMovieNodeExist = movieNode.records.length > 0;
			const isUserNodeExist = userNode.records.length > 0;

			if (!isMovieNodeExist) {
				await session.run('CREATE(m:Movie {title:$title,movieId:$movieId}) RETURN m', {
					title: movie.title,
					movieId: movie.id,
				});
			}

			if (!isUserNodeExist) {
				await session.run('CREATE(u:User {userId:$userId }) RETURN u', {
					userId,
				});
			}

			await session.run(
				'MATCH(a:User {userId:$userId}),(b:Movie {title:$title,movieId:$movieId}) MERGE (a)-[l:LIKE]->(b)',
				{
					userId,
					title: movie.title,
					movieId: movie.id,
				},
			);

			await session.close();
		} catch (e) {
			await session.close();
			throw e;
		}
	}
}

export const moviesService = new MoviesService();
