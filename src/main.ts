import { createApplication } from './app';
import { elasticService, neo4jService, prismaService } from './common/database';

async function bootstrap(): Promise<void> {
	const PID = process.pid;
	const PORT = process.env.PORT || 3000;

	await prismaService.connect();

	const app = createApplication();

	const server = app.listen(PORT, () => {
		console.log(`Server run on PORT=${PORT}; PID=${PID}`);
	});

	const onClose = (code: number): void => {
		server.close(async () => {
			await Promise.allSettled([
				prismaService.disconnect(),
				elasticService.disconnect(),
				neo4jService.disconnect(),
			]);

			process.exit(code);
		});
	};

	process.on('uncaughtException', onClose);
	process.on('unhandledRejection', onClose);
	process.on('SIGTERM', onClose);
	process.on('SIGINT', onClose);
}

bootstrap();
